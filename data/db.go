package data

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq"
	"log"
)

const (
	dbURL = "postgres://loraserver_as:loraserver_as" +
		"@207.154.224.20/loraserver_as?sslmode=disable"
)

var DB *sql.DB

func OpenDB() {
	var err error
	if DB, err = sql.Open("postgres", dbURL); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	fmt.Println(DB)
}

func GetDevices() (result []Node) {
	result = []Node{}
	rows, err := DB.Query("select application_id, name, iota, iota_sk from node where iota_sk <> ''")
	log.Println(rows)
	if err != nil {
		log.Fatal("1", err)
	}
	defer rows.Close()
	for rows.Next() {
		node := Node{}
		if err := rows.Scan(&node.ApplicationID, &node.Name, &node.Iota, &node.IotaSK); err != nil {
			log.Fatal(err)
		}
		result = append(result, node)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	return
}

type Node struct {
	ApplicationID          int64  `db:"application_id"`
	UseApplicationSettings bool   `db:"use_application_settings"`
	Name                   string `db:"name"`
	Description            string `db:"description"`
	//DevEUI                 lorawan.EUI64     `db:"dev_eui"`
	//AppEUI                 lorawan.EUI64     `db:"app_eui"`
	//AppKey                 lorawan.AES128Key `db:"app_key"`
	IsABP    bool `db:"is_abp"`
	IsClassC bool `db:"is_class_c"`
	//DevAddr                lorawan.DevAddr   `db:"dev_addr"`
	//NwkSKey                lorawan.AES128Key `db:"nwk_s_key"`
	//AppSKey                lorawan.AES128Key `db:"app_s_key"`
	//UsedDevNonces          DevNonceList      `db:"used_dev_nonces"`
	RelaxFCnt   bool   `db:"relax_fcnt"`
	Iota        string `db:"iota"`
	IotaSK      string `db:"iota_sk"`
	RXDelay     uint8  `db:"rx_delay"`
	RX1DROffset uint8  `db:"rx1_dr_offset"`
	RX2DR       uint8  `db:"rx2_dr"`

	ADRInterval        uint32  `db:"adr_interval"`
	InstallationMargin float64 `db:"installation_margin"`
	FirmwareVersion    string  `db:"firmware_version"`
}
